#'-------------------------------------------------------------------------#
#'-------------------------------------------------------------------------#
#'---------------- Cálculo de ángulos sólidos efectivos y -----------------#
#'-------- método de transferencia de eficiencia para detector NaI --------#
#'-------------------------------------------------------------------------#
#'-------------------------------------------------------------------------#

#'--------------------------------------------------------------------------
#'--Código          :   120123
#'--Fecha           :   12/01/23
#'--Usuarios        :   Paolo Tataje y Aldo Panfichi
#'--Descripción     :   Definir variables
#'--Status          :   Incompleto
#'--------------------------------------------------------------------------

import numpy as np
import pandas as pd
import time

''' SECCIÓN: PARAMETROS DE INPUT - GEOMETRÍA '''

n = 10#00000
rf = 4.6575 
hf = 2.5 
rd = 3.81 
hd = 3.81 

''' SECCIÓN: PARAMETROS DE INPUT - COEFICIENTES DE ATENUACIÓN '''

mu_f = 0.1393 # coeficiente de atenuacion lineal para la muestra a la energia de fotones muestreada (en cm^-1)

# LAS ENERGIAS DE INTERES:
# - 1.461 MeV (K-40): mu lineal para el SiO es 0.1393 cm-1
# - 1.764 MeV (Ra-226 -> Bi-214): mu lineal para el SiO es 0.1264 cm-1
# - 2.614 MeV (Th-232 -> 208 Tl): mu lineal para el SiO es 0.1032 cm-1


mu_d = 0.1724 # coeficiente de atenuacion lineal para el cristal del detector a la energia de fotones muestreada (en cm^-1)

# LAS ENERGIAS DE INTERES:
# - 1.461 MeV (K-40): mu lineal para el NaI es 0.1724 cm-1
# - 1.764 MeV (Ra-226 -> Bi-214): mu lineal para el NaI es 0.1585 cm-1
# - 2.614 MeV (Th-232 -> Tl-208): mu lineal para el NaI es 0.1389 cm-1


''' SECCIÓN: GENERACIÓN DE VALORES ALEATORIOS DE LOS FOTONES '''

# Generando los numeros aleatorios
Xi = np.random.rand(5, n)


# Generando las coordenadas de origen de los fotones dentro de la muestra
R = rf * np.sqrt(Xi[0])
X = R * np.cos(2*np.pi*Xi[1])
Y = R * np.sin(2*np.pi*Xi[1])
Z = hf * Xi[2]

# Generando los angulos de emision de los fotones
Phi = 2*np.pi*Xi[3]
Theta = np.arccos(2*Xi[4] - 1)


df = pd.DataFrame(np.zeros((n,6)), columns = ['Theta','Phi',
                                              'Se dirige al detector?',
                                              'd_muestra','d_aire','d_detector'])

df['Theta'] = Theta
df['Phi'] = Phi

''' SECCION: PARAMETROS DE INPUT PARA LAS CAPAS INTERMEDIAS '''

e_pp = 0.2 # espesor de la capa de pp (en cm) - usualmente es 0.2
e_al = 0.05 # espesor de la capa de aluminio (en cm) - usualmente es 0.05
e_mgo = 0.185 # espesor de la capa de mgo (en cm) - usualmente es 0.185

mu_pp = 0.05513 # coeficiente de atenuacion lineal de la capa de pp (en cm^-1)
mu_al = 0.1370 # coeficiente de atenuacion lineal de la capa de aluminio (en cm^-1)
mu_mgo = 0.1870 # coeficiente de atenuacion lineal de la capa de mgo (en cm^-1)


# LAS ENERGIAS DE INTERES:
# - 1.461 MeV (K-40): mu lineal para el PP/Al/Mgo son 0.05513/0.1370/0.1870 cm-1
# - 1.764 MeV (Ra-226 -> Bi-214): mu lineal para el PP/Al/Mgo son 0.04987/0.1243/0.1700 cm-1
# - 2.614 MeV (Th-232 -> 208 Tl): mu lineal para el PP/Al/Mgo son 0.04019/0.1022/0.1380 cm-1

''' SECCIÓN: CALCULOS DE ÁNGULOS Y DISTANCIAS VIAJADAS '''

tic = time.perf_counter() # para timear cuanto se demora el codigo en correr


for i in range(n):    
    #TESTING PURPOSES - escribe un ping cada 100000 fotones para mostrar que esta corriendo el bucle sin crashear
    if i%100000 == 0:
        print(i)


    # Identificamos en que cuadrante esta el punto de origen del foton (nos servira de utilidad mas adelante):
    if X[i] > 0 and Y[i] > 0:
        cuad = 1
    elif X[i] < 0 and Y[i] > 0:
        cuad = 2
    elif X[i] < 0 and Y[i] < 0:
        cuad = 3
    else:
        cuad = 4


    ##### 1ER CASO: rf >= rd & r < rd #####
    if rf >= rd and R[i] < rd:
        # Los valores criticos de theta para que el foton interactue con el detector:
        thetac = np.pi - np.arctan((rd + R[i]) / Z[i])
        
        print("thetaC: "+str(thetac))
        
        thetaf = np.pi - np.arctan((rd - R[i]) / Z[i])
        print("thetaF: "+str(thetaf))
        print(Theta[i])
        print('#--------------#')
        

        # si theta < thetac, nunca interactuara con el detecto
        if Theta[i] < thetac:
            continue

        elif Theta[i] >= thetaf:
            df['Se dirige al detector?'][i] = 1

        elif R[i] + Z[i]*np.tan(np.pi - Theta[i]) <= rd:
            df['Se dirige al detector?'][i] = 1
        
        else:
            print("next")

df.to_excel('resultados.xlsx')
        

    
























